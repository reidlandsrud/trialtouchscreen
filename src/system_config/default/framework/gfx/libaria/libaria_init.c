/*******************************************************************************
  MPLAB Harmony Graphics Composer Generated Implementation File

  File Name:
    libaria_init.c

  Summary:
    Build-time generated implementation from the MPLAB Harmony
    Graphics Composer.

  Description:
    Build-time generated implementation from the MPLAB Harmony
    Graphics Composer.

    Created with MPLAB Harmony Version 2.04
*******************************************************************************/
// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

#include "gfx/libaria/libaria_init.h"

laScheme defaultScheme;
laScheme NewScheme;
laScheme NewScheme_1;
laScheme CopyOfdefaultScheme;
laImageWidget* ImageWidget7;
laButtonWidget* ButtonWidget4;
laLabelWidget* LabelWidget8;
laLabelWidget* LabelWidget9;
laKeyPadWidget* KeyPadWidget1;
laTextFieldWidget* TextFieldWidget3;
laButtonWidget* ButtonWidget;
laButtonWidget* ButtonWidget1;
laButtonWidget* ButtonWidget2;
laListWidget* ListWidget3;


static void ScreenCreate_default(laScreen* screen);
static void ScreenCreate_screen1(laScreen* screen);
static void ScreenCreate_Menu(laScreen* screen);


int32_t libaria_initialize(void)
{
    laScreen* screen;

    laScheme_Initialize(&defaultScheme, GFX_COLOR_MODE_RGB_565);
    defaultScheme.base = 0xC67A;
    defaultScheme.highlight = 0xC67A;
    defaultScheme.highlightLight = 0xFFFF;
    defaultScheme.shadow = 0x8410;
    defaultScheme.shadowDark = 0x4208;
    defaultScheme.foreground = 0x0;
    defaultScheme.foregroundInactive = 0xD71C;
    defaultScheme.foregroundDisabled = 0x8410;
    defaultScheme.background = 0xFFFF;
    defaultScheme.backgroundInactive = 0xD71C;
    defaultScheme.backgroundDisabled = 0xA555;
    defaultScheme.text = 0x0;
    defaultScheme.textHighlight = 0x1F;
    defaultScheme.textHighlightText = 0xFFFF;
    defaultScheme.textInactive = 0xD71C;
    defaultScheme.textDisabled = 0x8C92;

    laScheme_Initialize(&NewScheme, GFX_COLOR_MODE_RGB_565);
    NewScheme.base = 0x0;
    NewScheme.highlight = 0xC67A;
    NewScheme.highlightLight = 0xFFFF;
    NewScheme.shadow = 0x8410;
    NewScheme.shadowDark = 0x4208;
    NewScheme.foreground = 0xF800;
    NewScheme.foregroundInactive = 0xF800;
    NewScheme.foregroundDisabled = 0x8410;
    NewScheme.background = 0xFE0;
    NewScheme.backgroundInactive = 0x7E0;
    NewScheme.backgroundDisabled = 0xC67A;
    NewScheme.text = 0x0;
    NewScheme.textHighlight = 0x1F;
    NewScheme.textHighlightText = 0xFFFF;
    NewScheme.textInactive = 0xD71C;
    NewScheme.textDisabled = 0x8C92;

    laScheme_Initialize(&NewScheme_1, GFX_COLOR_MODE_RGB_565);
    NewScheme_1.base = 0xC67A;
    NewScheme_1.highlight = 0xC67A;
    NewScheme_1.highlightLight = 0xFFFF;
    NewScheme_1.shadow = 0x8410;
    NewScheme_1.shadowDark = 0x4208;
    NewScheme_1.foreground = 0x0;
    NewScheme_1.foregroundInactive = 0xD71C;
    NewScheme_1.foregroundDisabled = 0x8410;
    NewScheme_1.background = 0x20;
    NewScheme_1.backgroundInactive = 0xD71C;
    NewScheme_1.backgroundDisabled = 0xC67A;
    NewScheme_1.text = 0x0;
    NewScheme_1.textHighlight = 0x1F;
    NewScheme_1.textHighlightText = 0xFFFF;
    NewScheme_1.textInactive = 0xD71C;
    NewScheme_1.textDisabled = 0x8C92;

    laScheme_Initialize(&CopyOfdefaultScheme, GFX_COLOR_MODE_RGB_565);
    CopyOfdefaultScheme.base = 0xD0E5;
    CopyOfdefaultScheme.highlight = 0xC67A;
    CopyOfdefaultScheme.highlightLight = 0xFFFF;
    CopyOfdefaultScheme.shadow = 0x8410;
    CopyOfdefaultScheme.shadowDark = 0x4208;
    CopyOfdefaultScheme.foreground = 0x0;
    CopyOfdefaultScheme.foregroundInactive = 0xD71C;
    CopyOfdefaultScheme.foregroundDisabled = 0x8410;
    CopyOfdefaultScheme.background = 0xFFFF;
    CopyOfdefaultScheme.backgroundInactive = 0xD71C;
    CopyOfdefaultScheme.backgroundDisabled = 0xC67A;
    CopyOfdefaultScheme.text = 0x0;
    CopyOfdefaultScheme.textHighlight = 0x1F;
    CopyOfdefaultScheme.textHighlightText = 0xFFFF;
    CopyOfdefaultScheme.textInactive = 0xD71C;
    CopyOfdefaultScheme.textDisabled = 0x8C92;

    screen = laScreen_New(LA_FALSE, LA_FALSE, &ScreenCreate_default);
    laContext_AddScreen(screen);

    screen = laScreen_New(LA_FALSE, LA_FALSE, &ScreenCreate_screen1);
    laContext_AddScreen(screen);

    screen = laScreen_New(LA_FALSE, LA_FALSE, &ScreenCreate_Menu);
    laContext_AddScreen(screen);

    GFX_Set(GFXF_DRAW_PIPELINE_MODE, GFX_PIPELINE_GCUGPU);
    laContext_SetStringTable(&stringTable);
    laContext_SetActiveScreen(0);

	return 0;
}

static void ScreenCreate_default(laScreen* screen)
{
    laLayer* layer0;

    layer0 = laLayer_New();
    laWidget_SetPosition((laWidget*)layer0, 0, 0);
    laWidget_SetSize((laWidget*)layer0, 480, 272);
    laWidget_SetBackgroundType((laWidget*)layer0, LA_WIDGET_BACKGROUND_FILL);
    laLayer_SetBufferCount(layer0, 1);

    laScreen_SetLayer(screen, 0, layer0);

    ImageWidget7 = laImageWidget_New();
    laWidget_SetPosition((laWidget*)ImageWidget7, 10, 1);
    laWidget_SetSize((laWidget*)ImageWidget7, 460, 270);
    laWidget_SetBackgroundType((laWidget*)ImageWidget7, LA_WIDGET_BACKGROUND_NONE);
    laWidget_SetBorderType((laWidget*)ImageWidget7, LA_WIDGET_BORDER_NONE);
    laImageWidget_SetImage(ImageWidget7, &logo);
    laWidget_AddChild((laWidget*)layer0, (laWidget*)ImageWidget7);

    ButtonWidget4 = laButtonWidget_New();
    laWidget_SetPosition((laWidget*)ButtonWidget4, 125, 201);
    laWidget_SetSize((laWidget*)ButtonWidget4, 239, 41);
    laWidget_SetBackgroundType((laWidget*)ButtonWidget4, LA_WIDGET_BACKGROUND_FILL);
    laWidget_SetBorderType((laWidget*)ButtonWidget4, LA_WIDGET_BORDER_BEVEL);
    laButtonWidget_SetText(ButtonWidget4, laString_CreateFromID(string_EnterID));
    laButtonWidget_SetPressedEventCallback(ButtonWidget4, &ButtonWidget4_PressedEvent);

    laWidget_AddChild((laWidget*)layer0, (laWidget*)ButtonWidget4);

    LabelWidget8 = laLabelWidget_New();
    laWidget_SetPosition((laWidget*)LabelWidget8, 100, 84);
    laWidget_SetSize((laWidget*)LabelWidget8, 280, 40);
    laWidget_SetBackgroundType((laWidget*)LabelWidget8, LA_WIDGET_BACKGROUND_NONE);
    laWidget_SetBorderType((laWidget*)LabelWidget8, LA_WIDGET_BORDER_NONE);
    laLabelWidget_SetText(LabelWidget8, laString_CreateFromID(string_ScanBadge));
    laWidget_AddChild((laWidget*)layer0, (laWidget*)LabelWidget8);

    LabelWidget9 = laLabelWidget_New();
    laWidget_SetPosition((laWidget*)LabelWidget9, 100, 146);
    laWidget_SetSize((laWidget*)LabelWidget9, 280, 25);
    laWidget_SetBackgroundType((laWidget*)LabelWidget9, LA_WIDGET_BACKGROUND_NONE);
    laWidget_SetBorderType((laWidget*)LabelWidget9, LA_WIDGET_BORDER_NONE);
    laLabelWidget_SetText(LabelWidget9, laString_CreateFromID(string_OrClick));
    laWidget_AddChild((laWidget*)layer0, (laWidget*)LabelWidget9);

}

static void ScreenCreate_screen1(laScreen* screen)
{
    laLayer* layer0;

    layer0 = laLayer_New();
    laWidget_SetPosition((laWidget*)layer0, 0, 0);
    laWidget_SetSize((laWidget*)layer0, 480, 272);
    laWidget_SetBackgroundType((laWidget*)layer0, LA_WIDGET_BACKGROUND_FILL);
    laWidget_SetScheme((laWidget*)layer0, &NewScheme_1);
    laLayer_SetBufferCount(layer0, 1);

    laScreen_SetLayer(screen, 0, layer0);

    KeyPadWidget1 = laKeyPadWidget_New(2, 5);
    laWidget_SetPosition((laWidget*)KeyPadWidget1, 0, 72);
    laWidget_SetSize((laWidget*)KeyPadWidget1, 400, 200);
    laWidget_SetBackgroundType((laWidget*)KeyPadWidget1, LA_WIDGET_BACKGROUND_NONE);
    laWidget_SetBorderType((laWidget*)KeyPadWidget1, LA_WIDGET_BORDER_BEVEL);
    laWidget_SetMargins((laWidget*)KeyPadWidget1, 4, 6, 6, 4);
    laKeyPadWidget_SetKeyText(KeyPadWidget1, 0, 0, laString_CreateFromID(string_Str1));
    laKeyPadWidget_SetKeyValue(KeyPadWidget1, 0, 0, laString_CreateFromID(string_Str1));
    laKeyPadWidget_SetKeyAction(KeyPadWidget1, 0, 0, LA_KEYPAD_CELL_ACTION_APPEND);
    laKeyPadWidget_SetKeyImagePosition(KeyPadWidget1, 0, 0, LA_RELATIVE_POSITION_BEHIND);
    laKeyPadWidget_SetKeyText(KeyPadWidget1, 0, 1, laString_CreateFromID(string_Str2));
    laKeyPadWidget_SetKeyValue(KeyPadWidget1, 0, 1, laString_CreateFromID(string_Str2));
    laKeyPadWidget_SetKeyAction(KeyPadWidget1, 0, 1, LA_KEYPAD_CELL_ACTION_APPEND);
    laKeyPadWidget_SetKeyImagePosition(KeyPadWidget1, 0, 1, LA_RELATIVE_POSITION_BEHIND);
    laKeyPadWidget_SetKeyText(KeyPadWidget1, 0, 2, laString_CreateFromID(string_Str3));
    laKeyPadWidget_SetKeyValue(KeyPadWidget1, 0, 2, laString_CreateFromID(string_Str3));
    laKeyPadWidget_SetKeyAction(KeyPadWidget1, 0, 2, LA_KEYPAD_CELL_ACTION_APPEND);
    laKeyPadWidget_SetKeyImagePosition(KeyPadWidget1, 0, 2, LA_RELATIVE_POSITION_BEHIND);
    laKeyPadWidget_SetKeyText(KeyPadWidget1, 0, 3, laString_CreateFromID(string_Str4));
    laKeyPadWidget_SetKeyValue(KeyPadWidget1, 0, 3, laString_CreateFromID(string_Str4));
    laKeyPadWidget_SetKeyAction(KeyPadWidget1, 0, 3, LA_KEYPAD_CELL_ACTION_APPEND);
    laKeyPadWidget_SetKeyImagePosition(KeyPadWidget1, 0, 3, LA_RELATIVE_POSITION_BEHIND);
    laKeyPadWidget_SetKeyText(KeyPadWidget1, 0, 4, laString_CreateFromID(string_Str5));
    laKeyPadWidget_SetKeyValue(KeyPadWidget1, 0, 4, laString_CreateFromID(string_Str5));
    laKeyPadWidget_SetKeyAction(KeyPadWidget1, 0, 4, LA_KEYPAD_CELL_ACTION_APPEND);
    laKeyPadWidget_SetKeyImagePosition(KeyPadWidget1, 0, 4, LA_RELATIVE_POSITION_BEHIND);
    laKeyPadWidget_SetKeyText(KeyPadWidget1, 1, 0, laString_CreateFromID(string_Str6));
    laKeyPadWidget_SetKeyValue(KeyPadWidget1, 1, 0, laString_CreateFromID(string_Str6));
    laKeyPadWidget_SetKeyAction(KeyPadWidget1, 1, 0, LA_KEYPAD_CELL_ACTION_APPEND);
    laKeyPadWidget_SetKeyImagePosition(KeyPadWidget1, 1, 0, LA_RELATIVE_POSITION_BEHIND);
    laKeyPadWidget_SetKeyText(KeyPadWidget1, 1, 1, laString_CreateFromID(string_Str7));
    laKeyPadWidget_SetKeyValue(KeyPadWidget1, 1, 1, laString_CreateFromID(string_Str7));
    laKeyPadWidget_SetKeyAction(KeyPadWidget1, 1, 1, LA_KEYPAD_CELL_ACTION_APPEND);
    laKeyPadWidget_SetKeyImagePosition(KeyPadWidget1, 1, 1, LA_RELATIVE_POSITION_BEHIND);
    laKeyPadWidget_SetKeyText(KeyPadWidget1, 1, 2, laString_CreateFromID(string_Str8));
    laKeyPadWidget_SetKeyValue(KeyPadWidget1, 1, 2, laString_CreateFromID(string_Str8));
    laKeyPadWidget_SetKeyAction(KeyPadWidget1, 1, 2, LA_KEYPAD_CELL_ACTION_APPEND);
    laKeyPadWidget_SetKeyImagePosition(KeyPadWidget1, 1, 2, LA_RELATIVE_POSITION_BEHIND);
    laKeyPadWidget_SetKeyText(KeyPadWidget1, 1, 3, laString_CreateFromID(string_Str9));
    laKeyPadWidget_SetKeyValue(KeyPadWidget1, 1, 3, laString_CreateFromID(string_Str9));
    laKeyPadWidget_SetKeyAction(KeyPadWidget1, 1, 3, LA_KEYPAD_CELL_ACTION_APPEND);
    laKeyPadWidget_SetKeyImagePosition(KeyPadWidget1, 1, 3, LA_RELATIVE_POSITION_BEHIND);
    laKeyPadWidget_SetKeyText(KeyPadWidget1, 1, 4, laString_CreateFromID(string_Str0));
    laKeyPadWidget_SetKeyValue(KeyPadWidget1, 1, 4, laString_CreateFromID(string_Str0));
    laKeyPadWidget_SetKeyAction(KeyPadWidget1, 1, 4, LA_KEYPAD_CELL_ACTION_APPEND);
    laKeyPadWidget_SetKeyImagePosition(KeyPadWidget1, 1, 4, LA_RELATIVE_POSITION_BEHIND);
    laKeyPadWidget_SetKeyClickEventCallback(KeyPadWidget1, &KeyPadWidget1_KeyClickEvent);

    laWidget_AddChild((laWidget*)layer0, (laWidget*)KeyPadWidget1);

    TextFieldWidget3 = laTextFieldWidget_New();
    laWidget_SetPosition((laWidget*)TextFieldWidget3, 75, 10);
    laWidget_SetSize((laWidget*)TextFieldWidget3, 397, 50);
    laWidget_SetBackgroundType((laWidget*)TextFieldWidget3, LA_WIDGET_BACKGROUND_FILL);
    laWidget_SetBorderType((laWidget*)TextFieldWidget3, LA_WIDGET_BORDER_BEVEL);
    laWidget_SetMargins((laWidget*)TextFieldWidget3, 8, 8, 8, 8);
    laTextFieldWidget_SetText(TextFieldWidget3, laString_CreateFromID(string_EmpID));
    laTextFieldWidget_SetAlignment(TextFieldWidget3, LA_HALIGN_CENTER);
    laTextFieldWidget_SetCursorEnabled(TextFieldWidget3, LA_TRUE);
    laTextFieldWidget_SetCursorDelay(TextFieldWidget3, 200);
    laTextFieldWidget_SetTextChangedEventCallback(TextFieldWidget3, &TextFieldWidget3_TextChangedEvent);
    laWidget_AddChild((laWidget*)layer0, (laWidget*)TextFieldWidget3);

    ButtonWidget = laButtonWidget_New();
    laWidget_SetPosition((laWidget*)ButtonWidget, 14, 10);
    laWidget_SetSize((laWidget*)ButtonWidget, 50, 50);
    laWidget_SetBackgroundType((laWidget*)ButtonWidget, LA_WIDGET_BACKGROUND_FILL);
    laWidget_SetBorderType((laWidget*)ButtonWidget, LA_WIDGET_BORDER_LINE);
    laButtonWidget_SetReleasedImage(ButtonWidget, &back_button);
    laButtonWidget_SetPressedEventCallback(ButtonWidget, &ButtonWidget_PressedEvent);

    laWidget_AddChild((laWidget*)layer0, (laWidget*)ButtonWidget);

    ButtonWidget1 = laButtonWidget_New();
    laWidget_SetPosition((laWidget*)ButtonWidget1, 400, 72);
    laWidget_SetSize((laWidget*)ButtonWidget1, 80, 100);
    laWidget_SetBackgroundType((laWidget*)ButtonWidget1, LA_WIDGET_BACKGROUND_FILL);
    laWidget_SetBorderType((laWidget*)ButtonWidget1, LA_WIDGET_BORDER_BEVEL);
    laButtonWidget_SetText(ButtonWidget1, laString_CreateFromID(string_CLR));
    laButtonWidget_SetPressedEventCallback(ButtonWidget1, &ButtonWidget1_PressedEvent);

    laWidget_AddChild((laWidget*)layer0, (laWidget*)ButtonWidget1);

    ButtonWidget2 = laButtonWidget_New();
    laWidget_SetPosition((laWidget*)ButtonWidget2, 400, 172);
    laWidget_SetSize((laWidget*)ButtonWidget2, 80, 100);
    laWidget_SetBackgroundType((laWidget*)ButtonWidget2, LA_WIDGET_BACKGROUND_FILL);
    laWidget_SetBorderType((laWidget*)ButtonWidget2, LA_WIDGET_BORDER_BEVEL);
    laButtonWidget_SetText(ButtonWidget2, laString_CreateFromID(string_Enter));
    laButtonWidget_SetPressedEventCallback(ButtonWidget2, &ButtonWidget2_PressedEvent);

    laWidget_AddChild((laWidget*)layer0, (laWidget*)ButtonWidget2);

}

static void ScreenCreate_Menu(laScreen* screen)
{
    laLayer* layer0;

    layer0 = laLayer_New();
    laWidget_SetPosition((laWidget*)layer0, 0, 0);
    laWidget_SetSize((laWidget*)layer0, 480, 272);
    laWidget_SetBackgroundType((laWidget*)layer0, LA_WIDGET_BACKGROUND_FILL);
    laLayer_SetBufferCount(layer0, 1);

    laScreen_SetLayer(screen, 0, layer0);

    ListWidget3 = laListWidget_New();
    laWidget_SetPosition((laWidget*)ListWidget3, 5, 52);
    laWidget_SetSize((laWidget*)ListWidget3, 315, 143);
    laWidget_SetBackgroundType((laWidget*)ListWidget3, LA_WIDGET_BACKGROUND_FILL);
    laWidget_SetBorderType((laWidget*)ListWidget3, LA_WIDGET_BORDER_BEVEL);
    laListWidget_SetSelectionMode(ListWidget3, LA_LIST_WIDGET_SELECTION_MODE_SINGLE);
    laListWidget_SetAllowEmptySelection(ListWidget3, LA_TRUE);
    laListWidget_AppendItem(ListWidget3);
    laListWidget_SetItemText(ListWidget3, 0, laString_CreateFromID(string_CLR));
    laListWidget_AppendItem(ListWidget3);
    laListWidget_SetItemText(ListWidget3, 1, laString_CreateFromID(string_Enter));
    laListWidget_AppendItem(ListWidget3);
    laListWidget_SetItemText(ListWidget3, 2, laString_CreateFromID(string_EmpID));
    laListWidget_AppendItem(ListWidget3);
    laListWidget_SetItemText(ListWidget3, 3, laString_CreateFromID(string_EnterID));
    laListWidget_AppendItem(ListWidget3);
    laListWidget_SetItemText(ListWidget3, 4, laString_CreateFromID(string_OrClick));
    laListWidget_AppendItem(ListWidget3);
    laListWidget_SetItemText(ListWidget3, 5, laString_CreateFromID(string_ScanBadge));
    laListWidget_AppendItem(ListWidget3);
    laListWidget_SetItemText(ListWidget3, 6, laString_CreateFromID(string_Str0));
    laListWidget_AppendItem(ListWidget3);
    laListWidget_SetItemText(ListWidget3, 7, laString_CreateFromID(string_Str1));
    laListWidget_AppendItem(ListWidget3);
    laListWidget_SetItemText(ListWidget3, 8, laString_CreateFromID(string_Str2));
    laWidget_AddChild((laWidget*)layer0, (laWidget*)ListWidget3);

}



