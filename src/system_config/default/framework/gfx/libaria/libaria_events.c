/*******************************************************************************
  MPLAB Harmony Graphics Composer Generated Implementation File

  File Name:
    libaria_events.c

  Summary:
    Build-time generated implementation from the MPLAB Harmony
    Graphics Composer.

  Description:
    Build-time generated implementation from the MPLAB Harmony
    Graphics Composer.

    Created with MPLAB Harmony Version 2.04
*******************************************************************************/
// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013-2014 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

#include "gfx/libaria/libaria_events.h"

// ButtonWidget4 - PressedEvent
void ButtonWidget4_PressedEvent(laButtonWidget* btn)
{
    // ShowSC1 - Show Screen - screen1
    laContext_SetActiveScreen(screen1_ID);
}

// KeyPadWidget1 - KeyClickEvent
void KeyPadWidget1_KeyClickEvent(laKeyPadWidget* pad, laButtonWidget* btn, uint32_t row, uint32_t col)
{
     // FocusOnTxt
    laContext_SetFocusWidget((laWidget*)TextFieldWidget3);
}

// TextFieldWidget3 - TextChangedEvent
void TextFieldWidget3_TextChangedEvent(laTextFieldWidget* txt)
{
    // noname - Show Screen - Menu
    laContext_SetActiveScreen(Menu_ID);
}

// ButtonWidget - PressedEvent
void ButtonWidget_PressedEvent(laButtonWidget* btn)
{
    // back - Show Screen - default
    laContext_SetActiveScreen(default_ID);
}

// ButtonWidget1 - PressedEvent
void ButtonWidget1_PressedEvent(laButtonWidget* btn)
{
    // FocusOnTxt
    laContext_SetFocusWidget((laWidget*)TextFieldWidget3);

    // Clear
    laEditWidget_Clear();
}

// ButtonWidget2 - PressedEvent
void ButtonWidget2_PressedEvent(laButtonWidget* btn)
{
    // ent
    laEditWidget_Accept();

    // Change screen - Show Screen - Menu
    laContext_SetActiveScreen(Menu_ID);
}





